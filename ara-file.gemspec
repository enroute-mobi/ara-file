require_relative 'lib/ara/version'

Gem::Specification.new do |spec|
  spec.name          = "ara-file"
  spec.version       = Ara::VERSION
  spec.authors       = ["enRoute"]
  spec.email         = ["development@enroute.mobi"]

  spec.summary       = %q{Export CSV file to be imported by ARA}
  spec.description   = %q{ara-file exports csv files that can be loaded in ARA to populate its models}
  spec.homepage      = "https://bitbucket.org/enroute-mobi/ara-file"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end

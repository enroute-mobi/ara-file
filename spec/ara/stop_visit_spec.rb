RSpec.describe Ara::StopVisit do
  it "should return the correct name" do
    expect(Ara::StopVisit.csv_name).to eq('stop_visit')
  end

  it "should return the correct default csv values" do
    sv = Ara::StopVisit.new
    expected = ['stop_visit', '', '', '{}', '', '', '', '[]', '{}', '{}']
    expect(sv.csv_attrs).to eq(expected)
  end

  it "should raise an exception with an incorrect type" do
    sv = Ara::StopVisit.new(objectids: 'wrong_value')
    expect{sv.csv_attrs}.to raise_error(Ara::InvalidAttributeError)
  end

  it "should return the correct values" do
    sv = Ara::StopVisit.new(
      id: 'id',
      model_name: 'model_name',
      objectids: {"a" => "b"},
      stop_area_id: 'stop_area_id',
      vehicle_journey_id: 'vehicle_journey_id',
      passage_order: "1",
      schedules: [{"a" => "b"}, {"c" => "d"}],
      attributes: {"a" => "b"},
      references: {"a" => "b"}
    )
    expected = ['stop_visit', 'id', 'model_name', '{"a":"b"}', 'stop_area_id', 'vehicle_journey_id', '1', '[{"a":"b"},{"c":"d"}]', '{"a":"b"}', '{"a":"b"}']
    expect(sv.csv_attrs).to eq(expected)
  end
end
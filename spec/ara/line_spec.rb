RSpec.describe Ara::Line do
  it "should return the correct name" do
    expect(Ara::Line.csv_name).to eq('line')
  end

  it "should return the correct default csv values" do
    line = Ara::Line.new(name: 'name')
    expected = ['line', '', '', 'name', '{}', '{}', '{}', true, '']
    expect(line.csv_attrs).to eq(expected)
  end

  it "should raise an exception with an incorrect type" do
    line = Ara::Line.new(objectids: 'wrong_value')
    expect{line.csv_attrs}.to raise_error(Ara::InvalidAttributeError)
  end

  it "should return the correct values" do
    line = Ara::Line.new(
      id: 'id',
      model_name: 'model_name',
      name: 'name',
      number: 'T4',
      objectids: {a: :b},
      attributes: {c: :d},
      references: {e: :f},
      collect_general_messages: false
    )
    expected = ['line', 'id', 'model_name', 'name', '{"a":"b"}', '{"c":"d"}', '{"e":"f"}', false, 'T4']
    expect(line.csv_attrs).to eq(expected)
  end
end
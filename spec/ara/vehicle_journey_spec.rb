RSpec.describe Ara::VehicleJourney do
  it "should return the correct name" do
    expect(Ara::VehicleJourney.csv_name).to eq('vehicle_journey')
  end

  it "should return the correct default csv values" do
    vj = Ara::VehicleJourney.new(name: 'name')
    expected = ['vehicle_journey', '', '', 'name', '{}', '', '', '', '{}', '{}', '']
    expect(vj.csv_attrs).to eq(expected)
  end

  it "should raise an exception with an incorrect type" do
    vj = Ara::VehicleJourney.new(objectids: 'wrong_value')
    expect{vj.csv_attrs}.to raise_error(Ara::InvalidAttributeError)
  end

  it "should return the correct values" do
    vj = Ara::VehicleJourney.new(
      id: 'id',
      name: 'name',
      model_name: 'model_name',
      objectids: {a: :b},
      line_id: 'line_id',
      origin_name: 'origin_name',
      destination_name: 'destination_name',
      direction_type: 'outbound',
      attributes: {c: :d},
      references: {e: :f},
    )
    expected = ['vehicle_journey', 'id', 'model_name', 'name', '{"a":"b"}', 'line_id', 'origin_name', 'destination_name', '{"c":"d"}', '{"e":"f"}', 'outbound']
    expect(vj.csv_attrs).to eq(expected)
  end
end
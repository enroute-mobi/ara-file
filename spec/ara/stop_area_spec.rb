RSpec.describe Ara::StopArea do
  it "should return the correct name" do
    expect(Ara::StopArea.csv_name).to eq('stop_area')
  end

  it "should return the correct default csv values" do
    sa = Ara::StopArea.new(name: 'name')
    expected = ['stop_area', '', '', '', '', 'name', '{}', '[]', '{}', '{}', true, false, true]
    expect(sa.csv_attrs).to eq(expected)
  end

  it "should raise an exception with an incorrect type" do
    sa = Ara::StopArea.new(objectids: 'wrong_value')
    expect{sa.csv_attrs}.to raise_error(Ara::InvalidAttributeError)
  end

  it "should return the correct values" do
    sa = Ara::StopArea.new(
      id: 'id',
      name: 'name',
      parent_id: 'parent_id',
      referent_id: 'referent_id',
      model_name: 'model_name',
      objectids: {a: :b},
      line_ids: ['line_id'],
      attributes: {c: :d},
      references: {e: :f},
      collected_always: false,
      collect_children: true,
      collect_general_messages: false
    )
    expected = ['stop_area', 'id', 'parent_id', 'referent_id', 'model_name', 'name', '{"a":"b"}', '["line_id"]', '{"c":"d"}', '{"e":"f"}', false, true, false]
    expect(sa.csv_attrs).to eq(expected)
  end
end
RSpec.describe Ara::File::Source do

  describe ".stop_areas" do
    subject { source.stop_areas.to_a }

    self::CSV = %{
    stop_area,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,2017-01-01,Name,"{""objectidKind"":""stopAreaObjectid""}","[""d0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11"",""e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11""]","{""AttributeKey"":""abcd""}","{""ReferenceType"":{""ObjectId"":{""objectidKind"":""5678""},""Id"":""42""}}",true,true,true

    }.strip

    context "when the file contains '#{self::CSV}'" do
      let(:content) { self.class::CSV }
      let(:file) { Tempfile.new.tap { |f| f.puts content; f.close }.path }
      let(:source) { Ara::File::Source.new(file) }

      it do
        stop_area = an_object_having_attributes(
          id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          parent_id: 'b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          referent_id: 'c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          model_name: '2017-01-01',
          name: 'Name',
          objectids: {"objectidKind" => "stopAreaObjectid"},
          line_ids: %w{
          d0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11
          },
          attributes: {"AttributeKey" => "abcd"},
          references: {"ReferenceType"=>{"ObjectId"=>{"objectidKind"=>"5678"}, "Id"=>"42"}},
          collected_always: true,
          collect_children: true,
          collect_general_messages: true
        )
        is_expected.to include(stop_area)
      end
    end
  end

  describe ".stop_visits" do
    subject { source.stop_visits.to_a }

    self::CSV = %{
      stop_visit,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11,2017-01-01,"{""objectidKind"":""VJObjectid""}",a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12,a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13,1,"[{""Kind"":""expected"",""ArrivalTime"":""2017-08-17T10:45:50+02:00"",""DepartureTime"":""2017-08-17T10:45:55+02:00""}]","{""AttributeKey"":""abcd""}","{""ReferenceType"":{""ObjectId"":{""objectidKind"":""5678""},""Id"":""42""}}"
    }.strip

    context "when the file contains '#{self::CSV}'" do
      let(:content) { self.class::CSV }
      let(:file) { Tempfile.new.tap { |f| f.puts content; f.close }.path }
      let(:source) { Ara::File::Source.new(file) }

      it do
        stop_visit = an_object_having_attributes(
          id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
          model_name: '2017-01-01',
          objectids: {"objectidKind" => "VJObjectid"},
          stop_area_id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12',
          vehicle_journey_id: 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a13',
          passage_order: "1",
          schedules: [{"Kind" => "expected", "ArrivalTime" => "2017-08-17T10:45:50+02:00", "DepartureTime" => "2017-08-17T10:45:55+02:00"}],
          attributes: {"AttributeKey" => "abcd"},
          references: {"ReferenceType" =>  {"ObjectId" => {"objectidKind" => "5678"}, "Id" =>"42"}}
        )
        is_expected.to include(stop_visit)
      end
    end
  end

end

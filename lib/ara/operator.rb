module Ara
  class Operator < Resource
    attribute :id
    attribute :model_name
    attribute :name
    attribute :objectids, kind: Hash, default: {}
  end
end

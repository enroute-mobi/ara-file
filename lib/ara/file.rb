require 'csv'

module Ara
  class File
    def initialize(csv_file_path)
      @csv_file_path = csv_file_path
      @model_names = {}
    end

    def model_name(day)
      @model_names[day] ||= Ara::ModelName.new(day)
      yield @model_names[day]
    end

    def close
      CSV.open(@csv_file_path, "wb") do |csv|
        @model_names.each do |_, model_name|
          model_name.export csv
        end
      end
    end

    class Source
      attr_reader :file_path
      def initialize(file_path)
        @file_path = file_path
      end

      def stop_areas
        ResourceReader.new(file_path, Ara::StopArea).resources
      end

      def lines
        ResourceReader.new(file_path, Ara::Line).resources
      end

      def operators
        ResourceReader.new(file_path, Ara::Operator).resources
      end

      def vehicle_journeys
        ResourceReader.new(file_path, Ara::VehicleJourney).resources
      end

      def stop_visits
        ResourceReader.new(file_path, Ara::StopVisit).resources
      end

      class ResourceReader
        attr_reader :file_path, :resource_class
        def initialize(file_path, resource_class)
          @file_path = file_path
          @resource_class = resource_class
        end

        def each(&block)
          CSV.foreach(file_path, "r") do |row|
            next unless row[0] == resource_class.csv_name
            block.call create(row)
          end
        end

        def csv_attribute_names
          @csv_attribute_names ||= resource_class.attributes.map(&:name)
        end

        def self.json_parser
          @json_parser = Proc.new { |csv_value| JSON.parse(csv_value) }
        end

        def csv_transformers
          @csv_transformers = resource_class.attributes.map do |attribute|
            if [Hash, Array, "Boolean"].include? attribute.kind
              self.class.json_parser
            end
          end
        end

        def create(row)
          raw_values = row[1..-1]

          transformers_and_values = [csv_transformers, raw_values].transpose
          transformed_values = transformers_and_values.map do |transformer, raw_value|
            transformer ? transformer.call(raw_value) : raw_value
          end
          attributes = [csv_attribute_names, transformed_values].transpose
          resource_class.new attributes
        end

        def resources
          enum_for :each
        end
      end
    end
  end
end

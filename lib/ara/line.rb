module Ara
  class Line < Resource
    attribute :id
    attribute :model_name
    attribute :name
    attribute :objectids, kind: Hash, default: {}
    attribute :attributes, kind: Hash, default: {}
    attribute :references, kind: Hash, default: {}
    attribute :collect_general_messages, kind: 'Boolean', default: true
    attribute :number
  end
end
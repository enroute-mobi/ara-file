# vehicle_journey,Id,ModelName,Name,ObjectIDs,LineId,OriginName,DestinationName,Attributes,References

module Ara
  class VehicleJourney < Resource
    attribute :id
    attribute :model_name
    attribute :name
    attribute :objectids, kind: Hash, default: {}
    attribute :line_id
    attribute :origin_name
    attribute :destination_name
    attribute :attributes, kind: Hash, default: {}
    attribute :references, kind: Hash, default: {}
    attribute :direction_type
  end
end
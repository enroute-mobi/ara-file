module Ara
   ENTITIES = [Ara::Operator, Ara::StopArea, Ara::Line, Ara::VehicleJourney, Ara::StopVisit]

  class ModelName
    def initialize(day)
      @day = day.strftime("%F")
      ENTITIES.each do |entity|
        instance_variable_set("@#{entity.csv_name}s", [] )

        define_singleton_method "#{entity.csv_name}s" do
          instance_variable_get("@#{entity.csv_name}s")
        end
      end
    end

    def add(resource)
      resource.model_name = @day
      send("#{resource.class.csv_name}s") << resource
    end
    alias << add

    def export(csv)
      ENTITIES.each do |entity|
        collection = instance_variable_get("@#{entity.csv_name}s")
        collection.each do |resource|
          csv << resource.csv_attrs
        end
      end
    end
  end
end
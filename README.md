# ara-file

A gem to generate CSV files to populate an ARA database

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ara-file'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install ara-file

## Usage

```ruby
target = Ara::File.new("output.csv")

(Date.today..Date.today+5).each do |day|
  target.model_name(day) do |model_name|

    model_name << Ara::StopArea.new(...)
    model_name << Ara::Line.new(...)
    model_name << Ara::VehicleJourney.new(...)
  end
end
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
